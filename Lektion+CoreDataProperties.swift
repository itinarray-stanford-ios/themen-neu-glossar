//
//  Lektion+CoreDataProperties.swift
//  ThemenNeuGlossar
//
//  Created by Dmitry Egunov on 2016-09-25.
//  Copyright © 2016 Itinarray. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Lektion {

    @NSManaged var book_id: NSNumber?
    @NSManaged var id: NSNumber?
    @NSManaged var name: String?

}
