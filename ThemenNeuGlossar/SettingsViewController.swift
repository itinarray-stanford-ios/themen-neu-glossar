//
//  SettingsViewController.swift
//  ThemenNeuGlossar
//
//  Created by D_E on 2016-08-30.
//  Copyright © 2016 Itinarray. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var labelForInternetStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Add Observer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SettingsViewController.reachabilityStatusChanged), name: "ReachStatusChanged", object: nil)
        
        self.reachabilityStatusChanged()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // Check if reachability status is changed
    func reachabilityStatusChanged(){
        if reachabilityStatus == Constants.InternetConnectionStatus.NotReachable
        {
            
            self.labelForInternetStatus.text = "You are Offline!"
            self.view.backgroundColor = UIColor.yellowColor()
        }
            
        else if reachabilityStatus == Constants.InternetConnectionStatus.ReachableViaWiFi
        {
            self.labelForInternetStatus.text = "Reachable via Wi-Fi!"
            self.view.backgroundColor = UIColor.greenColor()
        }
        else if reachabilityStatus == Constants.InternetConnectionStatus.ReachableViaWWAN{
            self.labelForInternetStatus.text = "Reachable via WWAN"
            self.view.backgroundColor = UIColor.cyanColor()
            
        }
    }
    
    // Called when class is deinitialized to remove Observer
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReachStatusChanged", object: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
