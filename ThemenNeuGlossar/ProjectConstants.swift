//
//  ProjectConstants.swift
//  ThemenNeuGlossar
//
//  Created by Kostiantyn Aleksieiev on 2016-08-29.
//  Copyright © 2016 Itinarray. All rights reserved.
//



class ProjectConstants {
    
    let MainNavTitle = "SELECT LEKTION"
    
    func mainTitle (lektionNum: String) -> String{
        return "LEKTION \(lektionNum)"
    }
}
