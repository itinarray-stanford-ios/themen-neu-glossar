//
//  ViewController2.swift
//  ThemenNeuGlossar
//
//  Created by Kostiantyn Aleksieiev on 2016-08-29.
//  Copyright © 2016 Itinarray. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TaskSelectorViewController: UIViewController {
    
    var entryId : Int?
    var mainTitle: String!
    var Constants = ProjectConstants()
    
    
    override func viewDidLoad() {
        
        mainTitle = Constants.mainTitle(String(entryId!))
        
        self.title = mainTitle
        
        print(mainTitle)
        
        
        Alamofire.request(.GET, "https://storage.googleapis.com/cdn1.smartychoice.com/newsletter/themen.json").responseJSON { (response) -> Void in
            
            // initialize json using SwiftyJSON
            if let value = response.result.value{
                
                let json = JSON(value)
                print("Object 1:")
                //                print("_id = \(json[0]["_id"])")
                //                print("book = \(json[0]["book"])")
                //                print("lektion = \(json[0]["lektion"])")
                //                print("object = \(json[0]["object"])")
                for (key, value) in json[0] {
                    if key == "_id" {
                        print(value)
                    }
                    print(key, value)
                }
                //                print(json[0])
                print("")
                
                print("Object 2:")
                print("_id = \(json[1]["_id"])")
                print("book = \(json[1]["book"])")
                print("lektion = \(json[1]["lektion"])")
                print("object = \(json[1]["object"])")
                print("")
                
                print("Object 3:")
                print("_id = \(json[2]["_id"])")
                print("book = \(json[2]["book"])")
                print("lektion = \(json[2]["lektion"])")
                print("object = \(json[2]["object"])")
                print("")
                
                print("Object 4:")
                print("_id = \(json[3]["_id"])")
                print("book = \(json[3]["book"])")
                print("lektion = \(json[3]["lektion"])")
                print("object = \(json[3]["object"])")
                print("")
                
                print("Object 5:")
                print("_id = \(json[4]["_id"])")
                print("book = \(json[4]["book"])")
                print("lektion = \(json[4]["lektion"])")
                print("object = \(json[4]["object"])")
                print("")
                
                print("Object 6:")
                print("_id = \(json[5]["_id"])")
                print("book = \(json[5]["book"])")
                print("lektion = \(json[5]["lektion"])")
                print("object = \(json[5]["object"])")
                print("")
                
            }
            
        }
        
    }
    

}
    

