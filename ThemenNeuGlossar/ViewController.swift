//
//  ViewController.swift
//  ThemenNeuGlossar
//
//  Created by Kostiantyn Aleksieiev on 2016-08-28.
//  Copyright © 2016 Itinarray. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ViewController: UIViewController {

    
    var lektionId : Int = 1
    
    
    @IBAction func tapOnLektion(sender: UIButton) {
        
        lektionId = Int(sender.restorationIdentifier!)!
        
        //print(String(lektionId))
        
        performSegueWithIdentifier("ShowTaskSelector", sender: self)
    }
    
    @IBAction func backFromTestsViewController(segue: UIStoryboardSegue){
        
    }
    
    @IBAction func backFromSettingsViewController(segue: UIStoryboardSegue){
        
    }
    
    @IBAction func backFromScoreViewController(segue: UIStoryboardSegue){
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if "ShowTaskSelector" == segue.identifier {
            // Nothing really to do here, since it won't be fired unless
            // shouldPerformSegueWithIdentifier() says it's ok. In a real app,
            // this is where you'd pass data to the success view controller.
            let destVC = segue.destinationViewController as! TaskSelectorViewController
            destVC.entryId = lektionId
         
        }
    }

}

