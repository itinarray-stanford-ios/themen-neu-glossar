//
//  Constants.swift
//  ThemenNeuGlossar
//
//  Created by Dmitry Egunov on 2016-09-01.
//  Copyright © 2016 Itinarray. All rights reserved.
//

import Foundation

struct Constants {
    
    struct InternetConnectionStatus {
        
        static let ReachableViaWiFi = "ReachableViaWiFi"
        static let NotReachable = "NotReachable"
        static let ReachableViaWWAN = "ReachableViaWWAN"
    }
    
    
}